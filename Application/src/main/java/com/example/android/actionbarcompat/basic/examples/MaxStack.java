package com.example.android.actionbarcompat.basic.examples;

import java.util.ArrayDeque;
import java.util.Deque;

public class MaxStack {

    private final int limit;

    private Deque<Integer> stack = new ArrayDeque<>();

    public MaxStack(int limit) {
        this.limit = limit;
    }

    public void push(int i) {
        if (stack.size() < limit) {
            stack.push(i);
            return;
        }
        throw new RuntimeException("Stack is full.");
    }

    public int pop(int i) {
        if (stack.size() > 0) {
            return stack.pop();
        }
        throw new RuntimeException("Stack is empty.");
    }
}
